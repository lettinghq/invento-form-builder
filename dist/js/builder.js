'use strict';

function InventoFormBuilder(json) {
    this.json           = {};
    this.form           = false;
    this.submitCallback = false;

    this.inputs = [
        {description : 'Short Text',        element : 'input',      type : 'text',      init : 'buildDefault'   },
        {description : 'Long Text',         element : 'textarea',   type :  null,       init : 'buildDefault'   },
        {description : 'Dropdown',          element : 'select',     type :  null,       init : 'buildDefault'   },
        {description : 'Email',             element : 'input',      type : 'email',     init : 'buildDefault'   },
        {description : 'Date',              element : 'input',      type : 'date',      init : 'buildDefault'   },
        {description : 'Date Range',        element : 'input',      type :  null,       init : 'buildDateRange' },
        {description : 'Website',           element : 'input',      type : 'url',       init : 'buildDefault'   },
        {description : 'Multiple Choice',   element : 'input',      type :  null,       init : 'buildCheckboxes'},
        {description : 'Boolean',           element : 'input',      type :  null,       init : 'buildRadios'    },
        {description : 'Switch',            element : 'input',      type : 'checkbox',  init : 'buildDefault'   },
        {description : 'Scale',             element : 'input',      type :  null,       init : 'buildScale'     },
        {description : 'Number',            element : 'input',      type : 'number',    init : 'buildDefault'   },
        {description : 'File Upload',       element : 'input',      type : 'file',      init : 'buildDefault'   },
    ];

    this.inputMapper = function(type) {
        return this.inputs.filter(function(input) {return input.description === type})[0];
    };

    this.acceptedTypes = function() {
        return this.inputs.map(function(input) {return input.description}).join(', ');
    };

    this.setAttributes = function(element, attributes) {
        var setter = function(attrs, set) {
            for (var property in attrs) {
                var attribute = attrs[property];

                if (attribute !== null) {
                    if (typeof attribute === 'object' && attribute.dataset === undefined && attribute[0] === undefined) {
                        setter(attribute, set[property]);
                    } else {
                        set[property] = attribute;
                    }
                }
            }
        };

        setter(attributes, element);
    };

    this.createElement = function(type, attributes) {
        var element = document.createElement(type);

        if (attributes && typeof attributes !== 'object') {
            throw Error('Attributes must be an object');
        }

        if (Object.keys(attributes).length > 0) {
            this.setAttributes(element, attributes);
        }

        return element;
    };

    this.uniqueId = function(base, id) {
        return base.replace(/ /g, '-').toLowerCase() + '-' + id;
    };

    this.randomInt = function() {
        return Math.round(Math.random() * 10000);
    };

    this.retrieveAnswers = function(e) {
        e.preventDefault();
        var answers = {}, form = document.forms[this.uniqueId('Generated Form', this.json.form.id)].elements;

        for (var i = 0; i < form.length - 1; i++) {
            this.json.questions[i].answer = form[i].value == '' ? null : form[i].value;
            answers[this.json.questions[i].reference] = this.json.questions[i].answer;
        }

        if (typeof this.submitCallback === 'function') {
            this.submitCallback.call(this, answers);
        } else {
            console.groupCollapsed('No callback method defined');
            console.log(answers);
            console.groupEnd();
        }
    };

    this.generateForm = function(json = false) {
        if (json !== false) {
            this.build(json);
            return this.form;
        } else {
            if (this.form !== false) {
                return this.form;
            } else {
                throw Error('No form has been created yet');
            }
        }
    };

    this.build = function(json) {
        var base;

        if (typeof json !== 'object') {
            throw Error('Build only accepts a JSON object');
        }

        this.json           = json;
        this.json.form.id   = this.randomInt();
        base                = this.base(this.json.form);

        for (var question in this.json.questions) {
            this.json.questions[question].id = this.randomInt();
            base.appendChild(this.input(this.json.questions[question]));
        }

        base.appendChild(this.buildSubmitButton());

        this.form = base;
    };

    this.base = function(base) {
        var form, attributes = base.attributes || {};

        attributes.id = this.uniqueId('Generated Form', base.id);
        attributes.onsubmit = this.retrieveAnswers.bind(this);

        form = this.createElement('form', attributes);

        if (base.title) {
            form.appendChild(this.createElement('h2', {
                innerText : base.title,
                className : 'form-heading'
            }));
        }

        return form;
    };

    this.input = function(question) {
        var input, wrapper, label,
            idBase = this.uniqueId(question.type, question.id),
            type   = this.inputMapper(question.type);

        if (!type) {
            throw Error('Invalid input type. Accepted types are ' + this.acceptedTypes());
        }

        label = this.createElement('label', {
            id          : 'label-' + idBase,
            htmlFor     : 'input-' + idBase,
            innerText   : question.title,
            className   : 'control-label'
        });

        input = this[type.init](type, question, idBase);

        wrapper = this.createElement('div', {id : 'wrapper-' + idBase});

        if (type.init === 'buildDefault') {
            this.setAttributes(wrapper, {className : 'form-group'});
        }

        wrapper.appendChild(label);
        wrapper.appendChild(input);

        return wrapper;
    };

    this.buildSubmitButton = function() {
        var wrapper = this.createElement('div', {className : 'form-group'});

        wrapper.appendChild(this.createElement('button', {
            type : 'submit',
            innerText : this.json.form.submit_message ? this.json.form.submit_message : 'Submit',
            className : 'btn btn-primary'
        }));

        return wrapper;
    };

    this.buildDefault = function(type, question, idBase) {
        var input, inputWrapper, subInput;

        input = this.createElement(type.element, {
            id          : 'input-' + idBase,
            name        : 'input-' + idBase,
            placeholder : question.title,
            pattern     : question.pattern || null,
            required    : question.required,
            type        : type.type,
            className   : 'form-control'
        });

        if (question.attributes) {
            this.setAttributes(input, question.attributes);
        }

        if (question.options) {
            if (type.element === 'select') {
                for (var option in question.options) {
                    subInput = this.createElement('option', {
                        value           : question.options[option].value,
                        innerText       : question.options[option].title,
                        defaultSelected : question.options[option].default
                    });

                    input.appendChild(subInput);
                }
            }
        }

        inputWrapper = this.createElement('div', {id : 'input-wrapper-' + idBase});
        inputWrapper.appendChild(input);

        return inputWrapper;
    };

    this.buildCheckboxes = function(type, question, idBase) {
        var optionLabel, optionInput, wrapper;

        wrapper = this.createElement('div', {
            id          : 'input-wrapper-' + idBase,
            className   : 'form-group'
        });

        for (var option in question.options) {
            optionInput = this.createElement('input', {
                defaultChecked  : question.options[option].default,
                id              : 'input-' + idBase + '-' + option,
                name            : 'input-' + idBase + '-' + option,
                type            : 'checkbox'
            });

            optionLabel = this.createElement('label', {
                innerHTML   : optionInput.outerHTML + question.options[option].title,
                id          : 'label-' + idBase + '-' + option,
                htmlFor     : 'input-' + idBase + '-' + option,
                className   : 'control-label'
            });

            wrapper.appendChild(optionLabel);
        }

        return wrapper;
    };

    this.buildDateRange = function(type, question, idBase) {
        var wrapper, fromWrapper, toWrapper;

        wrapper     = this.createElement('div', {id : 'input-wrapper-' + idBase});
        fromWrapper = this.createElement('div', {id : 'input-wrapper-' + idBase + '-0', className : 'form-group'});
        toWrapper   = this.createElement('div', {id : 'input-wrapper-' + idBase + '-1', className : 'form-group'});

        fromWrapper.appendChild(this.createElement('label', {
            id          : 'label-' + idBase + '-from',
            htmlFor     : 'input-' + idBase + '-from',
            className   : 'control-label',
            innerText   : 'From'
        }));

        fromWrapper.appendChild(this.createElement('input', {
            id          : 'input-' + idBase + '-from',
            name        : 'input-' + idBase + '-from',
            type        : 'date',
            className   : 'form-control'
        }));

        toWrapper.appendChild(this.createElement('label', {
            id          : 'label-' + idBase + '-to',
            htmlFor     : 'input-' + idBase + '-to',
            className   : 'control-label',
            innerText   : 'To'
        }));

        toWrapper.appendChild(this.createElement('input', {
            id          : 'input-' + idBase + '-to',
            name        : 'input-' + idBase + '-to',
            type        : 'date',
            className   : 'form-control'
        }));

        wrapper.appendChild(fromWrapper);
        wrapper.appendChild(toWrapper);

        return wrapper;
    };

    this.buildRadios = function(type, question, idBase) {
        var wrapper     = this.createElement('div',   {id : 'input-wrapper-' + idBase, className : 'form-group'}),
            yesRadio    = this.createElement('input', {id : 'input-' + idBase + '-yes', name : 'input-' + idBase, type : 'radio'}),
            yesLabel    = this.createElement('label', {id : 'label-' + idBase + '-yes', innerHTML : yesRadio.outerHTML + ' Yes', className : 'control-label'}),
            noRadio     = this.createElement('input', {id : 'input-' + idBase + '-no', name : 'input-' + idBase, type : 'radio'}),
            noLabel     = this.createElement('label', {id : 'label-' + idBase + '-no', innerHTML : yesRadio.outerHTML + ' No', className : 'control-label'});

        wrapper.appendChild(yesLabel);
        wrapper.appendChild(noLabel);

        return wrapper
    };

    this.buildScale = function(type, question, idBase) {
        var scaleRadio, scaleLabel, wrapper,
            min = parseInt(question.attributes.minimum),
            max = parseInt(question.attributes.maximum);

        wrapper = this.createElement('div', {id : 'input-wrapper-' + idBase, className : 'form-group'});

        for (var i = min; i <= max; i++) {
            scaleRadio = this.createElement('input', {id : 'input-' + idBase + '-' + i, name : 'input-' + idBase, type : 'radio'});
            scaleLabel = this.createElement('label', {id : 'label-' + idBase + '-' + i, innerHTML: scaleRadio.outerHTML + ' ' + i, className : 'control-label'});

            wrapper.appendChild(scaleLabel);
        }

        return wrapper;
    };
};
